# Set up bus and bridge.
bus = calamity.global()
bridge = new calamity.WorkerBridge bus
bridge.on "error", (msg) -> console.error "[worker]", msg.data
bridge.on "log", (msg) -> console.log "[worker]", msg.data
# Wait for bridge to connect.
bridge.on "connect", ->
	console.log "[frame] bridge connected"
	bus.publish "test.frame.ready"

# Subscriptions.
subs = []

# Relay data back via another publish.
subs.push bus.subscribe "test.publish1.request", (msg) ->
	bus.publish "test.publish1.reply", msg.data

# Two handlers registered to the same address.
bus.subscribe "test.send1.request", -> bus.publish "test.send1.reply"
bus.subscribe "test.send1.request", -> bus.publish "test.send1.reply"

# Reply tests for publish and send.
bus.subscribe "test.reply1.request", (msg) ->
	msg.reply "#{msg.data}-reply"

# Unsubscribe everything when finish signal is received.
subs.push bus.subscribe "test.frame.finish", ->
	for sub in subs
		sub.unsubscribe()
