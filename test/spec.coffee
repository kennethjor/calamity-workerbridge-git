describe "WorkerBridge", ->
	# Set up the bus and bridge. These are global and will not be reset on every test.
	bus = calamity.global()
	frameConnected = false
	bus.subscribe "test.frame.ready", ->
		frameConnected = true
	bridge = new calamity.WorkerBridge bus
	bridge.on "error", (msg) -> console.error "[worker]", msg.data
	bridge.on "log", (msg) -> console.log "[worker]", msg.data

	it "should be supported", ->
		expect(calamity.WorkerBridge.isSupported()).toBe true

	it "should connect with bridge", (done) ->
		if bridge.connected
			expect(bridge.connected).toBe true
			done()
		else
			bridge.on "connect", ->
				expect(bridge.connected).toBe true
				done()

	# This test will block and wait for the frame to report back.
	# Whether this method will remain reliable in the future, I'm not sure.
	it "should connect to the frame", (done) ->
		test = ->
			if frameConnected
				expect(frameConnected).toBe true
				done()
			else
				_.defer test
		test()

	it "should publish messages between busses", (done) ->
		# When the frame receives a test.publish1.request, it will publish a test.publish1.reply with the same data.
		bus.subscribe "test.publish1.reply", (msg) ->
			expect(msg.address).toBe "test.publish1.reply"
			expect(msg.data).toBe "foo"
			done()
		bus.publish "test.publish1.request", "foo"

	xit "should send messages to a single handler", (done) ->
		# Two handlers are registered for test.send1.request. When received they will publish on test.send1.reply.
		# Only one of the handlers should execute.
		counter = 0
		bus.subscribe "test.send1.reply", ->
			counter++
			# When first reply is received, wait a bit and make sure only one handler was executed.
			if counter is 1
				_.delay (->
					expect(counter).toBe 1
					done()
				), 100
		bus.send "test.send1.request"

	it "should reply to publishes", (done) ->
		counter = 0
		bus.publish "test.reply1.request", "data", (reply) ->
			counter++
			expect(counter).toBe 1
			expect(reply.data).toBe "data-reply"
			_.delay done, 100

	xit "should reply to sends", (done) ->
		counter = 0
		bus.send "test.reply1.request", "data", (reply) ->
			counter++
			expect(counter).toBe 1
			expect(reply.data).toBe "data-reply"
			_.delay done, 100

	xit "should preserve the message ID throughout"
