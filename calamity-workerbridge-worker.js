/*! Calamity Worker Bridge (worker) 0.0.1 - MIT license */
(function(){
var handlePublish, handleReply, log, ports, postMessage, replyHandlers,
  __hasProp = {}.hasOwnProperty;

ports = [];

replyHandlers = [];

self.addEventListener("connect", function(event) {
  var port;
  port = event.ports[0];
  ports.push(port);
  port.addEventListener("message", (function(port) {
    return function(msg) {
      var data, e, json;
      data = msg.data;
      if (data === "ping") {
        port.postMessage("pong");
      }
      try {
        json = JSON.parse(data);
        switch (json.type) {
          case "publish":
            handlePublish(port, json.type, json.msg);
            break;
          case "reply":
            handleReply(json.replyId, json.msg);
        }
      } catch (_error) {
        e = _error;
        postMessage(port, "error", e.stack);
      }
    };
  })(port));
  port.start();
  port.postMessage("pong");
});

handlePublish = function(port, type, msg) {
  var p, _i, _len;
  if (msg.reply != null) {
    replyHandlers[msg.reply] = port;
  }
  for (_i = 0, _len = ports.length; _i < _len; _i++) {
    p = ports[_i];
    if (p !== port) {
      postMessage(p, type, msg);
    }
  }
};

handleReply = function(replyId, msg) {
  var port;
  port = replyHandlers[replyId];
  if (port == null) {
    throw new Error("No reply handler for replyId:" + replyId);
  }
  postMessage(port, "reply", msg, {
    replyId: replyId
  });
};

log = function(msg) {
  var p, str, _i, _len;
  str = JSON.stringify({
    type: "log",
    msg: msg
  });
  for (_i = 0, _len = ports.length; _i < _len; _i++) {
    p = ports[_i];
    p.postMessage(str);
  }
};

postMessage = function(port, type, msg, args) {
  var json, k, v;
  if (args == null) {
    args = {};
  }
  json = {
    type: type,
    msg: msg
  };
  for (k in args) {
    if (!__hasProp.call(args, k)) continue;
    v = args[k];
    json[k] = v;
  }
  port.postMessage(JSON.stringify(json));
};
}).call(this);
//# sourceMappingURL=calamity-workerbridge-worker.js.map