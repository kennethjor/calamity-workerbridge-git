/*! Calamity Worker Bridge 0.0.1 - MIT license */
(function(){
// Import calamity if necessary.
if (typeof calamity === "undefined" && typeof require === "function") {
	calamity = require("calamity");
}
var __hasProp = {}.hasOwnProperty,
  __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

calamity.WorkerBridge = (function(_super) {
  __extends(WorkerBridge, _super);

  WorkerBridge.version = "0.0.1";

  calamity.emitter(WorkerBridge.prototype);

  WorkerBridge.prototype.connected = false;

  WorkerBridge.prototype._workerUrl = null;

  WorkerBridge.prototype._worker = null;

  WorkerBridge.prototype._replyHandlers = null;

  function WorkerBridge(bus, workerUrl) {
    if (workerUrl == null) {
      workerUrl = "calamity-workerbridge-worker.js";
    }
    if (!calamity.WorkerBridge.isSupported()) {
      throw new Error("Shared workers not supported");
    }
    WorkerBridge.__super__.constructor.call(this, bus);
    this._replyHandlers = {};
    this._workerUrl = workerUrl;
    this._initWorker();
  }

  WorkerBridge.prototype._initWorker = function() {
    var port, worker;
    worker = this._worker = new SharedWorker(this._workerUrl);
    port = worker.port;
    port.addEventListener("message", ((function(_this) {
      return function(msg) {
        _this._handleMessage(msg);
      };
    })(this)), false);
    port.start();
  };

  WorkerBridge.prototype.handle = function(bus, msg) {
    if (bus == null) {
      return WorkerBridge.__super__.handle.call(this, null, msg);
    }
    if (this.seen(msg)) {
      return;
    }
    return this._postMessage("publish", msg);
  };

  WorkerBridge.prototype._postMessage = function(type, msg, args) {
    var json, replyId;
    if (args == null) {
      args = {};
    }
    json = msg.toJSON();
    if (json.reply != null) {
      replyId = calamity.util.genId();
      this._replyHandlers[replyId] = json.reply;
      json.reply = replyId;
    }
    this._worker.port.postMessage(JSON.stringify(_.extend({}, args, {
      type: type,
      msg: json
    })));
  };

  WorkerBridge.prototype._handleMessage = function(msg) {
    var data, e, json, reply;
    data = msg.data;
    if (data === "pong") {
      if (!this.connected) {
        this.connected = true;
        this.trigger("connect");
      }
      return;
    }
    try {
      json = JSON.parse(data);
    } catch (_error) {
      e = _error;
      throw new SyntaxError("JSON parse failed: " + data);
    }
    switch (json.type) {
      case "log":
        this.trigger("log", json.msg);
        break;
      case "error":
        this.trigger("error", json.msg);
        break;
      case "publish":
        if (json.msg.reply != null) {
          json.msg.reply = this._createReplyWrapper(json.msg.reply);
        }
        this.handle(null, calamity.Message.fromJSON(json.msg));
        break;
      case "reply":
        reply = this._replyHandlers[json.replyId];
        if (typeof reply === "function") {
          reply(calamity.Message.fromJSON(json.msg));
        }
    }
  };

  WorkerBridge.prototype._createReplyWrapper = function(replyId) {
    return (function(_this) {
      return function(replyMsg) {
        _this._postMessage("reply", replyMsg, {
          replyId: replyId
        });
      };
    })(this);
  };

  WorkerBridge.isSupported = function() {
    return !!window.SharedWorker;
  };

  return WorkerBridge;

})(calamity.Bridge);
}).call(this);
//# sourceMappingURL=calamity-workerbridge.js.map