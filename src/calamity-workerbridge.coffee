class calamity.WorkerBridge extends calamity.Bridge
	@version: "%version%"
	calamity.emitter @prototype
	# Whether the bridge is connected or not.
	connected: false
	# The URL of the worker script.
	_workerUrl: null
	# The SharedWorker instance.
	_worker: null
	# Reply handlers for messages send through.
	_replyHandlers: null

	# Constructs the new bridge.
	constructor: (bus, workerUrl = "calamity-workerbridge-worker.js") ->
		throw new Error "Shared workers not supported" unless calamity.WorkerBridge.isSupported()
		super bus
		@_replyHandlers = {}
		# Init SharedWorker.
		@_workerUrl = workerUrl
		@_initWorker()

	# Initialises the SharedWorker instance.
	_initWorker: ->
		worker = @_worker = new SharedWorker @_workerUrl
		port = worker.port
		# Attach message handler.
		port.addEventListener "message", ((msg) =>
			@_handleMessage msg
			return
		), false
		# Start port.
		port.start()
		return

	# Handles a message coming from the bus.
	handle: (bus, msg) ->
		# If bus is not set, it means it's coming from the worker. Send it to the super class for processing.
		unless bus?
			return super null, msg
		# Message came from the bus, send it to the worker.
		return if @seen msg
		@_postMessage "publish", msg

	# Posts a message to the worker.
	_postMessage: (type, msg, args = {}) ->
		# Serialize message.
		json = msg.toJSON()
		# Extract reply handler and replace it with a unique ID.
		if json.reply?
			replyId = calamity.util.genId()
			@_replyHandlers[replyId] = json.reply
			json.reply = replyId
		# Send message to worker.
		#console.log "PostMessage:", json
		@_worker.port.postMessage JSON.stringify _.extend {}, args,
			type: type
			msg: json
		return

	# Handles a message coming in from the worker.
	_handleMessage: (msg) ->
		{data} = msg
		# When a pong is received, register the connection as active.
		if data is "pong"
			if !@connected
				@connected = true
				@trigger "connect"
			return
		# Deserialize message.
		try
			json = JSON.parse data
		catch e
			throw new SyntaxError "JSON parse failed: #{data}"
		# Handle message types.
		switch json.type
			# Worker logging. This is only used for development purposes.
			when "log"
				@trigger "log", json.msg
			# Worker errors.
			when "error"
				@trigger "error", json.msg
			# Published messages.
			when "publish"
				# Create reply wrapper.
				if json.msg.reply?
					json.msg.reply = @_createReplyWrapper json.msg.reply
				# Send to handler.
				@handle null, calamity.Message.fromJSON json.msg
			# Replies.
			when "reply"
				# Load original reply handler.
				reply = @_replyHandlers[json.replyId]
				reply? calamity.Message.fromJSON json.msg

		return

	_createReplyWrapper: (replyId) -> (replyMsg) =>
		@_postMessage "reply", replyMsg, {replyId}
		return

	# Returns true is shared web workers are supported.
	@isSupported = -> return !!window.SharedWorker
