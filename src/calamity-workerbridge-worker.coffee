# Ports container.
ports = []
# Known reply IDs and their mapping to specific ports.
replyHandlers = []

# When new connections are received.
self.addEventListener "connect", (event) ->
	# Add port to register.
	port = event.ports[0]
	ports.push port
	# Attach message handler for this port.
	port.addEventListener "message", do (port) -> (msg) ->
		{data} = msg
		# Answer pings.
		if data is "ping" then port.postMessage "pong"
		# Wrap processing of the message and relay errors back to the specific port
		try
			json = JSON.parse data
			#log "Worker handling message: [#{json.type}] #{JSON.stringify json}"
			switch json.type
				when "publish" then handlePublish port, json.type, json.msg
				when "reply" then handleReply json.replyId, json.msg
		catch e
			postMessage port, "error", e.stack
		return
	port.start()
	# Send pong to indicate successful load.
	port.postMessage "pong"
	return

# Handles publish messages.
# This will relay the message to all ports, except the one it came from.
handlePublish = (port, type, msg) ->
	# Save reply ID for later reference.
	if msg.reply?
		replyHandlers[msg.reply] = port
	# Send to all ports.
	for p in ports
		unless p is port
			postMessage p, type, msg
	return

# Handles reply messages.
handleReply = (replyId, msg) ->
	# Get port for reply ID.
	port = replyHandlers[replyId]
	# Send message to port if we know it.
	unless port?
		throw new Error "No reply handler for replyId:#{replyId}"
	postMessage port, "reply", msg, {replyId}
	return

log = (msg) ->
	str = JSON.stringify
		type: "log"
		msg: msg
	for p in ports
		p.postMessage str
	return

postMessage = (port, type, msg, args = {}) ->
	json = {type, msg}
	for own k, v of args
		json[k] = v
	port.postMessage JSON.stringify json
	return
